FROM papierpain/nextcord:1.3.0

LABEL maintainer="PapierPain <gourves.seven@gmail.com>"
LABEL description="Discord container for Botte Peuplade"

####################
# CONFIGURATION
####################

WORKDIR /bot

COPY ./src /bot

RUN SODIUM_INSTALL=system python3 -m pip install --no-cache-dir --break-system-packages -r ./requirements.txt

CMD ["python3", "./main.py"]
