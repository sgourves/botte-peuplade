# Botte Peuplade

Bot Botte pour le serveur discord **LaPeuplade**.

## Developpement

Installer les requirements :

```bash
pip3 install -r local.requirements.txt
```

Créer le fichier .env :

```bash
# BOT
BOTTE_ENV=<development|production>
BOTTE_VERSION=<version-du-bot>
BOTTE_TOKEN=<token-du-bot-discord>
BOTTE_PREFIX=/
# GUILD
GUILD_ID=<id-de-votre-serveur-discord>
# BLAGUES
BLAGUE_API_TOKEN=<token-de-l-api-de-blagues>
# NUAGE
PLP_API_TOKEN=<token-pour-les-sons>
```

Lancer le bot :

```bash
python3 main.py
```

## Tutoriel secrets avec Helm

### Pré-requis (local)

Installer SOPS :

```bash
# From: https://github.com/getsops/sops/releases

# Download the binary
curl -LO https://github.com/getsops/sops/releases/download/v3.9.0/sops-v3.9.0.linux.amd64

# Move the binary in to your PATH
sudo mv sops-v3.9.0.linux.amd64 /usr/local/bin/sops

# Make the binary executable
sudo chmod +x /usr/local/bin/sops
```

Installer Age :

```bash
sudo apt install age
```

### Générer une clé Age

```bash
mkdir -p ~/.age-keys
age-keygen -o ~/.age-keys/botte-peuplade.key
```

### Chiffrer le fichier de secrets

Créer le fichier de values (avec les mots de passe), nommé par exemple `secrets.yaml`.
Puis le chiffrer :

```bash
sops -e --age=<public-age-key> secrets.yaml > secrets.enc.yaml
```

### Configurer les variables GitLab CI

Dans Settings > CI/CD > Variables de votre projet GitLab :

> AGE_PRIVATE_KEY : Contenu de `~/.age-keys/botte-peuplade.key` (clé privée).

### Intégrer Age dans GitLab CI

Ajoutez un job pour déchiffrer le fichier avec Age dans votre `.gitlab-ci.yml` :

```yaml
Decrypt Secrets:
  stage: prepare
  image: chatwork/sops:3.9.0
  script:
    - echo "$AGE_PRIVATE_KEY" > .age-key.key
    - sops -d --age-key .age-key.key secrets.enc.yaml > secrets.yaml

Deploy App:
  stage: deploy
  extends: .helm-template
  script:
    - helm upgrade --install ${CI_PROJECT_NAME} login-project/${PACKAGE_NAME} --values secrets.yaml
```
