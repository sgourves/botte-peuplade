import nextcord
from nextcord import Interaction, SlashOption
from nextcord.ext import commands

from libs.utils.constants import Bot, Colors, ZorblortAPI
from libs.utils.logger import create_logger
from libs.utils.messages import MessageType
from libs.utils.zorblort.zorblort import Zorblort

ICON = "👻"
ZORBLORT_ICON = (
    "https://gitlab.com/uploads/-/system/project/avatar/59292236/zorblort.jpg?width=96"
)
DEFAULT_MODEL = "llama3.1"


class CogInvocation(commands.Cog, description="Invocation de Zorplort"):
    """Invocation de Zorplort"""

    def __init__(self, bot: commands.Bot) -> None:
        """Initialisation du Cog

        Parameters
        ----------
        bot: commands.Bot
            Bot
        """

        self.bot: commands.Bot = bot
        self.zorblort = Zorblort(ZorblortAPI.HOST)

        self._logger = create_logger(self.__class__.__name__)
        self._logger.info(f"{self.__class__.__name__} chargé")

    async def _zorblort_message(
        self,
        interaction: Interaction,
        message: str,
        personnage: str,
        history: bool = False,
    ) -> None:
        """Message à envoyer à Zorblort

        Parameters
        ----------
        interaction: Interaction
            Interaction du slash command
        message: str
            Message à envoyer à Zorblort
        personnage: str
            Personnage à incarner
        history: bool
            Historique de la conversation

        Returns
        -------
        dict
            Réponse de Zorblort
        """

        self._logger.debug(f"Method called {self._zorblort_message.__name__}")

        await interaction.response.defer()

        try:
            zorblort_response = self.zorblort.message(
                message=message, model=personnage, history=history
            )

            self._logger.info(f"Zorblort API response: {zorblort_response}")

            full_message = f"> *{message}*\n\n{zorblort_response['message']}"

            embed = nextcord.Embed(description=full_message, color=Colors.INFO)
            embed.set_author(icon_url=ZORBLORT_ICON, name=personnage)
            embed.set_footer(
                text=f"Temps : {round(zorblort_response["duration"], 2)} secondes"
            )
            await interaction.send(embed=embed)
        except Exception as e:
            self._logger.error(f"Error while calling Zorblort API: {e}")
            await MessageType.error(
                interaction, f"Erreur lors de l'invocation: {e}", ICON
            )

    @nextcord.slash_command(
        name="invocation",
        description="INVOCAAAAAAAAAAAAAAATIONNNNNN.... ᵖᵒᵘᶠ",
        guild_ids=Bot.GUILDS,
    )
    async def invocation(
        self,
        interaction: Interaction,
        message: str = SlashOption(
            name="message", description="Message à envoyer à Zorblort", required=True
        ),
        personnage: str = SlashOption(
            name="personnage",
            description="Personnage à incarner",
            default=DEFAULT_MODEL,
        ),
    ) -> None:
        """Commande d'invocation

        Parameters
        ----------
        interaction: Interaction
            Interaction du slash command
        personnage: str
            Personnage à incarner
        message: str
            Message à envoyer à Zorblort
        """

        self._logger.debug(f"Slash command {self.invocation.name} called")

        await self._zorblort_message(interaction, message, personnage, history=True)

    @nextcord.slash_command(
        name="rick-sanchez",
        description="Invocation de Rick Sanchez",
        guild_ids=Bot.GUILDS,
    )
    async def rick_sanchez(
        self,
        interaction: Interaction,
        message: str = SlashOption(
            name="message",
            description="Message à envoyer à Rick Sanchez",
            required=True,
        ),
    ) -> None:
        """Commande d'invocation

        Parameters
        ----------
        interaction: Interaction
            Interaction du slash command
        message: str
            Message à envoyer à Zorblort
        """

        self._logger.debug(f"Slash command {self.invocation.name} called")

        await self._zorblort_message(interaction, message, "rick-sanchez", history=True)

    @nextcord.slash_command(
        name="models",
        description="Récupération des models de Zorblort",
        guild_ids=Bot.GUILDS,
    )
    async def models(self, interaction: Interaction) -> None:
        """Récupération des models de Zorblort

        Parameters
        ----------
        interaction: Interaction
            Interaction du slash command.
        """

        self._logger.debug(f"Slash command {self.models.name} called")

        await interaction.response.defer()

        try:
            models: list = self.zorblort.list_models()

            embed = nextcord.Embed(
                title="Liste des models disponibles", color=Colors.INFO
            )

            for model in models:
                model_info = model["model"]
                if model["parent"] != "":
                    model_info += f" (parent: {model["parent"]})"

                embed.add_field(name=model["name"], value=model_info, inline=False)

            embed.set_author(icon_url=ZORBLORT_ICON, name="Zorblort")
            await interaction.send(embed=embed)
        except Exception as e:
            self._logger.error(f"Error while getting models: {e}")
            await MessageType.error(
                interaction, f"Erreur lors de la récupération des models : {e}", ICON
            )

    @commands.has_permissions(administrator=True)
    @nextcord.slash_command(
        name="create-model",
        description="Création d'un nouveau modèle de Zorblort",
        guild_ids=Bot.GUILDS,
    )
    async def create_model(
        self,
        interaction: Interaction,
        model: str = SlashOption(
            name="model", description="Nom du modèle", required=True
        ),
        prompt: str = SlashOption(
            name="prompt", description="Prompt du modèle", required=True
        ),
        parent: str = SlashOption(
            name="parent", description="Parent du modèle", default="llama3.1"
        ),
    ) -> None:
        """Ajout d'un model de Zorblort

        Parameters
        ----------
        interaction: Interaction
            Interaction du slash command.
        model: str
            Nom du modèle
        prompt: str
            Prompt du modèle
        parent: str
            Parent du modèle
        """

        self._logger.debug(f"Slash command {self.create_model.name} called")

        await interaction.response.defer()

        try:
            self.zorblort.add_model(model=model, prompt_system=prompt, parent=parent)

            await MessageType.info(interaction, f"Modèle {model} ajouté", ICON)
        except Exception as e:
            self._logger.error(f"Error while creating model {model}: {e}")
            await MessageType.error(
                interaction, f"Erreur lors de la création du model {model} : {e}", ICON
            )

    @commands.has_permissions(administrator=True)
    @nextcord.slash_command(
        name="delete-model",
        description="Suppression d'un modèle de Zorblort",
        guild_ids=Bot.GUILDS,
    )
    async def delete_model(
        self,
        interaction: Interaction,
        model: str = SlashOption(
            name="model", description="Nom du modèle", required=True
        ),
    ) -> None:
        """Suppression d'un model de Zorblort

        Parameters
        ----------
        interaction: Interaction
            Interaction du slash command.
        model: str
            Nom du modèle
        """

        self._logger.debug(f"Slash command {self.delete_model.name} called")

        await interaction.response.defer()

        try:
            self.zorblort.delete_model(model)

            await MessageType.info(interaction, f"Modèle {model} supprimé", ICON)
        except Exception as e:
            self._logger.error(f"Error while deleting model {model}: {e}")
            await MessageType.error(
                interaction,
                f"Erreur lors de la suppression du model {model} : {e}",
                ICON,
            )

    @nextcord.slash_command(
        name="nettoyage-historique",
        description="Nettoyage de l'historique de Zorblort",
        guild_ids=Bot.GUILDS,
    )
    async def nettoyage_historique(
        self,
        interaction: Interaction,
        personnage: str = SlashOption(
            name="personnage", description="Personnage à nettoyer"
        ),
    ) -> None:
        """Nettoyage de l'historique de Zorblort

        Parameters
        ----------
        interaction: Interaction
            Interaction du slash command
        personnage: str
            Personnage à nettoyer
        """

        self._logger.debug(f"Slash command {self.nettoyage_historique.name} called")

        await interaction.response.defer()

        try:
            self.zorblort.clean_history(personnage)
            await MessageType.info(interaction, "Historique nettoyé", ICON)
        except Exception as e:
            self._logger.error(f"Error while cleaning Zorblort history: {e}")
            await MessageType.error(
                interaction, f"Erreur lors du nettoyage de l'historique: {e}", ICON
            )


def setup(bot: commands.Bot):
    bot.add_cog(CogInvocation(bot))
