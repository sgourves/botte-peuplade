import os
from dotenv import load_dotenv

load_dotenv()


class Bot:
    """Classe des constantes du Botte

    Attributes
    ----------
    TOKEN (str): Token du bot
    PREFIX (str): Préfixe des commandes
    GUILDS (list): Liste des guilds autorisées
    """

    __slots__ = ()
    TOKEN: str = os.getenv("BOTTE_TOKEN")
    PREFIX: str = os.getenv("BOTTE_PREFIX")
    GUILDS: list = [int(os.getenv("GUILD_ID"))]


class Blagues:
    """Classe des constantes de l'API de blagues

    Attributes
    ----------
    TOKEN (str): Token de l'API de blagues
    """

    __slots__ = ()
    TOKEN: str = os.getenv("BLAGUE_API_TOKEN")


class ZorblortAPI:
    """Classe des constantes de l'API de Zorblort

    Attributes
    ----------
    HOST (str): URL de l'API de Zorblort
    """

    __slots__ = ()
    HOST: str = os.getenv("ZORBLORT_API")


class Colors:
    """Classe des couleurs

    Attributes
    ----------
    INFO (int): Couleur bleue
    ERROR (int): Couleur rouge
    WARNING (int): Couleur orange
    SUCCESS (int): Couleur verte
    """

    __slots__ = ()
    INFO: int = 0x00CCB2
    ERROR: int = 0xF23F42
    WARNING: int = 0xFFAA00
    SUCCESS: int = 0x00CC66
