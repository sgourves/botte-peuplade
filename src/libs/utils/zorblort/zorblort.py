from ollama import Client, ResponseError

from libs.utils.logger import create_logger


class Zorblort:
    """Intéraction avec l'API du grand et magnifique Zorblort.

    Parameters
    ----------
    """

    def __init__(self, api_host: str) -> None:
        """Initialisation de la classe."""

        self._logger = create_logger(self.__class__.__name__)
        self.client = Client(host=api_host)
        self.context_history = {}

        self._logger.info(f"{self.__class__.__name__} loaded: {api_host}")

    def message(self, message: str, model: str, history: bool = False) -> dict:
        """Envoie un message à Zorblort.

        Parameters
        ----------
        message: str
            Message à envoyer à Zorblort.
        model: str
            Modèle à utiliser.
        history: bool
            Historique des échanges.

        Returns
        -------
        dict
            Réponse de Zorblort.
        """

        self._logger.debug(f"Method called {self.message.__name__}")
        self._logger.info(f"Zorblort API request [model: {model}]: {message}")

        response = None
        context = None

        if history and model in self.context_history:
            context = self.context_history[model]

        try:
            return_data = self.client.generate(
                model=model, prompt=message, context=context
            )

            self._logger.debug(f"Zorblort API response: {return_data}")

            response = {
                "message": return_data["response"],
                "duration": return_data["total_duration"] / 1_000_000_000,
            }

            if history:
                self.context_history[model] = return_data["context"]

        except ResponseError as e:
            self._logger.error(f"Error while chatting with Zorblort: {e.error}")

        return response

    def list_models(self) -> list:
        """Get all present models on Zorblort server"""

        self._logger.debug(f"Method called {self.list_models.__name__}")

        models = self.client.list()
        self._logger.debug(f"Models returned: {models}")

        # Extract information
        filtered_models = [
            {
                "name": model["name"],
                "model": model["model"],
                "parent": model["details"]["parent_model"],
            }
            for model in models["models"]
        ]
        self._logger.debug(f"Models filtered: {filtered_models}")

        return filtered_models

    def add_model(
        self, model: str, prompt_system: str = "", parent: str = "llama3.1"
    ) -> None:
        """Add a model on Zorblort server

        Parameters
        ----------
        model: str
            Nom du modèle
        prompt_system: str
            Prompt du système
        parent: str
            Modèle de référence
        """

        self._logger.debug(f"Method called {self.add_model.__name__}")

        modelfile: str = f"FROM {parent}"

        if prompt_system != "":
            modelfile += f"\nSYSTEM {prompt_system}"

        self.client.create(model=model, modelfile=modelfile)

    def delete_model(self, model: str) -> None:
        """Delete a model on Zorblort server

        Parameters
        ----------
        model: str
            Nom du modèle
        """

        self._logger.debug(f"Method called {self.delete_model.__name__}")

        self.client.delete(model)

    def clean_history(self, model: str) -> None:
        """Nettoie l'historique des échanges.

        Parameters
        ----------
        model: str
            Modèle à nettoyer.
        """

        self._logger.debug(f"Method called {self.clean_history.__name__}")

        if model in self.context_history:
            self.context_history.pop(model)
            self._logger.info(f"History cleaned for model: {model}")
        else:
            self._logger.error(f"Model not found in history: {model}")
            raise ValueError(f"Model not found in history: {model}")
